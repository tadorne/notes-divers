---
title: Customization de Windows
date: 2019-12-29
---

# Avoir un look "classique" pour Windows 7 et 10

Pour retrouver un look "classique avec Windows 10 ou Window 7, il va être nécessaire de télécharger quelques utilitaires et ressources :

1. Les utilitaires
  - Open-Shell
  - Le pack Tango icons
2. Fonds d'écrans
  - les images de fond
  - les codes couleurs
3. la barre de lancement rapide
  - rendre visible la *Quicklaunch*

Installer les utilitaires :

- [Open-Shell](https://github.com/Open-Shell/Open-Shell-Menu) (successeur du [Classic Shell](http://www.classicshell.net/)), un menu démarrer classique avec différents types de présentations et offrant de multiples options.
- [Tango icons pack by ](https://www.deviantart.com/alexgal23/art/Tango-IconPack-Installer-527398763), le pack remplace les icônes système par celles du [projet Tango](https://en.wikipedia.org/wiki/Tango_Desktop_Project)
  - <https://en.wikipedia.org/wiki/Tango_Desktop_Project>

---


Utilitaire pour changer l'image de boot W7 :

http://www.coderforlife.com/projects/win7boot/


Un avis sur l'interface de W95 :

https://twitter.com/tuomassalo/status/978717292023500805

https://socket3.wordpress.com/2018/02/03/designing-windows-95s-user-interface/


Activer le soulignement des lettres avec raccourci clavier dans les menus des logiciels :

https://twitter.com/fiecila/status/1131623092298633217

Avec W7 :
<https://www.sevenforums.com/tutorials/178303-keyboard-shortcuts-access-keys-underline.html>

Avec W10 aussi, inclut changement via la base de registre :

<https://www.tenforums.com/tutorials/97413-turn-off-underline-access-key-shortcuts-menus-windows-10-a.html>

---

Le vert de Windows 95 :

| 120 | 0   |
| 240 | 128 |
| 60  | 128 |

Hexcode : #008080


OldNewExplorer :

https://tihiy.net/files/OldNewExplorer.rar

https://msfn.org/board/topic/170375-oldnewexplorer-119

