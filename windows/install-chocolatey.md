---
title: Installation de Chocolatey
---

A coller dans une fenêtre Powershell en administrateur :

`Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`

Autorise l'execution du script distant d'installation du gestionnaire de paquets Chocolatey