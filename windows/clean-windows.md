---
title: Nettoyage du système (Windows)
---

# Nettoyer le cache de Chocolatey

1. installer l'utilitaire *choco-cleaner* : `choco install choco-cleaner`
2. lancer la tâche via Powershell : `Set-ExecutionPolicy -ExecutionPolicy Unrestricted
\ProgramData\chocolatey\bin\Choco-Cleaner.ps1`

[How to clear Chocolatey cache in the free version?](https://superuser.com/questions/1371668/how-to-clear-chocolatey-cache-in-the-free-version)
