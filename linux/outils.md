---
title: Outils sous Linux
date: 2019-12-31
---

Remind - un agenda en CLI

- [Site ressource Linux Remind](https://dianne.skoll.ca/projects/remind/)
- [Article du Linux Journal](https://www.linuxjournal.com/article/3529)
- [un tuto pour gérer les anniversaires](http://voidandany.free.fr/index.php/pour-ne-plus-jamais-oublier-un-anniversaire-remind/)

Worklog

Un outil de suivi de ses activités (informatiques). Il existe une *[Dockapp](https://en.wikipedia.org/wiki/Dockapp)* pour l'environnement de bureau Window Maker pour gérer ses journaux de suivi de façon graphique.

Pour l'installer sous Debian et les distribution dérivées : `sudo apt-get install worklog`

Installateur de tarball

[Linux Liar](https://framagit.org/grumpyf0x48/liar)