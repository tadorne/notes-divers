---
title: Choisir sa distribution et son environnement de bureau
date: 2019-12-31
---

# Choisir un système adapté à ses habitudes et ses besoins

Choisir un système adapté à ses besoins. ici seront présentées des distributions prêtes à l'emploi avec différentes variantes dans les gestionnaires de bureau, avec différents réglages pour s'adapter à différentes habitudes de travail et d'utilisation.

L'ensemble des choix proposés sont issus de la "famille" Debian et Ubuntu. On pourra trouver des assemblages équivalent dans d'autres types de *distribution*.

Déjà, comment classer les types de "bureaux" ?

- en faisant référence à leurs possibles sources d'inspiration ?
- par la configuration adoptée
- par la technologie utilisée ?

## Debian Facile

Le menu des logiciels se situe en bas de l'écran, à gauche, avec des icônes bien visibles et des règlages assez faciles à faire (lanceurs d'applications, ajouts de fonctionnalités dans les barres d'outils).

Il s'agit ici de XFCE réglé "à Windows 7"

---

# Sources

https://wiki.archlinux.org/index.php/Desktop_environment#Xfce_packages

https://en.wikipedia.org/wiki/Desktop_environment

https://mygeekopinions.blogspot.com/2011/08/lxde-vs-xfce.html


https://www.noobslab.com/2017/01/lumina-desktop-can-be-installed-on-your.html
https://sparkylinux.org/lumina-desktop-moved-to-sparky-repos/

